package com.root55.youkal.Setting;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.navigation.NavigationView;

import com.root55.youkal.Home.Home;
import com.root55.youkal.Models.Setting_model;
import com.root55.youkal.R;
import com.root55.youkal.adapters.Setting_adapter;
import com.root55.youkal.edit_profile.Edit_Profile;
import com.root55.youkal.favorite.Favorite_Screen;

import java.util.ArrayList;

public class Setting extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener , View.OnClickListener , Setting_adapter.SelectedItem {
    private static final String EDIT_PROFILE = "EDIT_PROFILE" ;
    private static final String CHANGE_LANGUAGE = "CHANGE_LANGUAGE" ;
    private static final String CONTACT_US = "CONTACT_US" ;
    private static final String SHARE_APP = "SHARE_APP" ;
    private static final String RATE_APP = "RATE_APP" ;
    private static final String LOG_OUT = "LOG_OUT" ;

    private Setting_adapter.SelectedItem mSelectedItem ;
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private RecyclerView mSetting_rv;
    private Setting_adapter mAdapter;
    private RecyclerView.LayoutManager mManger;
    private ArrayList<Setting_model> mSettingModel;
    private ImageView mMenu ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        initData();
        initViews();
    }

    private void initData() {
        mSettingModel = new ArrayList<>();
        mSettingModel.add(new Setting_model(R.drawable.edit_profile, EDIT_PROFILE, R.drawable.go_to_next));
        mSettingModel.add(new Setting_model(R.drawable.change_language, CHANGE_LANGUAGE, R.drawable.go_to_next));
        mSettingModel.add(new Setting_model(R.drawable.contact_us, CONTACT_US, R.drawable.go_to_next));
        mSettingModel.add(new Setting_model(R.drawable.share_app, SHARE_APP, R.drawable.go_to_next));
        mSettingModel.add(new Setting_model(R.drawable.rate_app, RATE_APP, R.drawable.go_to_next));
        mSettingModel.add(new Setting_model(R.drawable.logout, LOG_OUT, R.drawable.go_to_next));
    }

    private void initViews() {
        mMenu = findViewById(R.id.menu) ;
        mMenu.setOnClickListener(this);

        mDrawer = findViewById(R.id.setting_drawer);
        mNavigationView = findViewById(R.id.setting_nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        mSetting_rv = findViewById(R.id.rv_setting);
        mAdapter = new Setting_adapter(mSettingModel , this);
        mManger = new LinearLayoutManager(Setting.this, RecyclerView.VERTICAL, false);

        mSetting_rv.setAdapter(mAdapter);
        mSetting_rv.setLayoutManager(mManger);

    }

    private void OpenDrawer() {
        mDrawer.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case  R.id.nav_Home :
                startActivity(new Intent(Setting.this, Home.class));

                break;
            case R.id.nav_profile:

                break;
            case R.id.nav_favorites:
                startActivity(new Intent(Setting.this, Favorite_Screen.class));
                break;
            case R.id.setting:
                startActivity(new Intent(Setting.this, Setting.class));
                break;
            case R.id.log_out:
                break;
        }
        mDrawer.closeDrawer(Gravity.LEFT);
        return true;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.menu :
                OpenDrawer();
                break;
        }
    }


    @Override
    public void SelectedTitle(String title) {
        if(title.equalsIgnoreCase(EDIT_PROFILE)){
            startActivity(new Intent(Setting.this , Edit_Profile.class));
        }
    }
}
