package com.root55.youkal.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.root55.youkal.Auth.Pojos.AuthResponse;
import com.root55.youkal.Home.Home;
import com.root55.youkal.R;
import com.root55.youkal.UserSession.User_Session;
import com.root55.youkal.WepService.ApiService;
import com.root55.youkal.WepService.WepServiceClient;
import com.root55.youkal.forget_password.Forget_Password;

import java.util.Objects;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SignUp extends AppCompatActivity implements View.OnClickListener {
    private Button mSignIn, mLogIn;
    private EditText mFirstname, mLastname, mEmail, mPhone, mPassword;
    private TextView mForget_Password;
    private User_Session mUserSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initViews();

    }

    private void initViews() {
        mSignIn = findViewById(R.id.sign_in);
        mLogIn = findViewById(R.id.log_in);
        mUserSession = new User_Session(SignUp.this);
        mFirstname = findViewById(R.id.first_name);
        mLastname = findViewById(R.id.last_name);
        mEmail = findViewById(R.id.email);
        mPhone = findViewById(R.id.phone);
        mPassword = findViewById(R.id.pass_word);

        mForget_Password = findViewById(R.id.forget_password);
        mSignIn.setOnClickListener(this);
        mLogIn.setOnClickListener(this);
        mForget_Password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in:
                if (CheckValidations()) {
                    CallRegisterApi();
                }

                break;
            case R.id.log_in:
                startActivity(new Intent(SignUp.this, Login.class));
                break;
            case R.id.forget_password:
                startActivity(new Intent(SignUp.this, Forget_Password.class));
                break;
        }
    }

    private void CallRegisterApi() {

        ApiService service = WepServiceClient.getRetrofit().create(ApiService.class);
        Call<AuthResponse> call = service.Register(mFirstname.getText().toString(), mLastname.getText().toString()
                , mPhone.getText().toString(), mEmail.getText().toString(), mPassword.getText().toString());
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                Log.e("success", response.body().getMsg());
                if (response.body().getStatus()) {

                    mUserSession.SignIn(mEmail.getText().toString(), mPassword.getText().toString(), mPhone.getText().toString()
                            , mFirstname.getText().toString(), mLastname.getText().toString(), true);
                 //   mUserSession.addUserId(response.body().getResult().get(0).getId());
                    Log.e("userId" , response.body().getResult().get(0).getId());
                    startActivity(new Intent(SignUp.this, Home.class));
                    SignUp.this.finish();
                } else {
                    Toast.makeText(SignUp.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                }

            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Log.e("error", Objects.requireNonNull(t.getMessage()));
            }
        });
    }

    private boolean CheckValidations() {
        if (mEmail.getText().toString().isEmpty()) {
            Toast.makeText(SignUp.this, "please enter your email ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPassword.getText().toString().isEmpty()) {
            Toast.makeText(SignUp.this, "please enter your password ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mFirstname.getText().toString().isEmpty()) {
            Toast.makeText(SignUp.this, "please enter your first name ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mLastname.getText().toString().isEmpty()) {
            Toast.makeText(SignUp.this, "please enter your last name ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPhone.getText().toString().isEmpty()) {
            Toast.makeText(SignUp.this, "please enter your phone ", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }

    }
}
