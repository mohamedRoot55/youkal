package com.root55.youkal.Auth;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.root55.youkal.Auth.Pojos.AuthResponse;
import com.root55.youkal.Home.Home;
import com.root55.youkal.R;
import com.root55.youkal.UserSession.User_Session;
import com.root55.youkal.WepService.ApiService;
import com.root55.youkal.WepService.WepServiceClient;
import com.root55.youkal.forget_password.Forget_Password;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Login extends AppCompatActivity implements View.OnClickListener {
    private Button mSignIn, mLogIn;
    private EditText mEmail, mPassword;
    private TextView mForget_Password;
    private User_Session mUserSession;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initViews();
    }

    private void initViews() {
        mSignIn = findViewById(R.id.sign_in);
        mForget_Password = findViewById(R.id.forget_password);
        mLogIn = findViewById(R.id.log_in);
        mEmail = findViewById(R.id.email);
        mPassword = findViewById(R.id.pass_word);

        mUserSession = new User_Session(Login.this);

        mLogIn.setOnClickListener(this);
        mSignIn.setOnClickListener(this);
        mForget_Password.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.sign_in:
                startActivity(new Intent(Login.this, SignUp.class));
                break;
            case R.id.forget_password:
                startActivity(new Intent(Login.this, Forget_Password.class));
                break;
            case R.id.log_in:
                if (CheckValidations()) {
                  CallLoginApi() ;
                }
                break;
        }
    }

    private void CallLoginApi() {
        ApiService apiService = WepServiceClient.getRetrofit().create(ApiService.class) ;
        Call<AuthResponse> call = apiService.Login(mEmail.getText().toString() , mPassword.getText().toString());
        call.enqueue(new Callback<AuthResponse>() {
            @Override
            public void onResponse(Call<AuthResponse> call, Response<AuthResponse> response) {
                Log.e("success" , response.body().getMsg() ) ;
                if(response.body().getStatus()){
                    mUserSession.Login(mEmail.getText().toString() , mPassword.getText().toString() , true);
                    startActivity(new Intent(Login.this , Home.class));
                    Login.this.finish();
                }else {
                    Toast.makeText(Login.this, response.body().getMsg(), Toast.LENGTH_LONG).show();

                }
            }

            @Override
            public void onFailure(Call<AuthResponse> call, Throwable t) {
                Log.e("success" , t.getMessage()) ;
            }
        });
    }

    private boolean CheckValidations() {
        if (mEmail.getText().toString().isEmpty()) {
            Toast.makeText(Login.this, "please enter your email ", Toast.LENGTH_SHORT).show();
            return false;
        } else if (mPassword.getText().toString().isEmpty()) {
            Toast.makeText(Login.this, "please enter your password ", Toast.LENGTH_SHORT).show();
            return false;
        } else {
            return true;
        }

    }
}
