package com.root55.youkal.Home;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;

import androidx.recyclerview.widget.RecyclerView;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.navigation.NavigationView;
import com.root55.youkal.Home.Pojos.CategoryResponse;
import com.root55.youkal.Home.Pojos.Result;
import com.root55.youkal.Location.MapsActivity;
import com.root55.youkal.R;
import com.root55.youkal.Setting.Setting;
import com.root55.youkal.WepService.ApiService;
import com.root55.youkal.WepService.WepServiceClient;
import com.root55.youkal.adapters.Cat_adapter;
import com.root55.youkal.favorite.Favorite_Screen;


import java.util.ArrayList;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Home extends AppCompatActivity implements View.OnClickListener, NavigationView.OnNavigationItemSelectedListener {
    private DrawerLayout mDrawer;
    private NavigationView mNavigationView;
    private ImageView mMenu , mMore_Vert;

    private PopupWindow mDropdown = null;
    private LayoutInflater mLayoutinfalter;

    private RecyclerView mCat_rv;
    private RecyclerView.Adapter mCat_adapter;
    private RecyclerView.LayoutManager manager;
    private ArrayList<Result> mData = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initViews();
        initData();
    }

    private void initData() {
        CallGetCategoryApi();
    }

    private void CallGetCategoryApi() {
        ApiService apiService = WepServiceClient.getRetrofit().create(ApiService.class);
        Call<CategoryResponse> call = apiService.getCategory();
        call.enqueue(new Callback<CategoryResponse>() {
            @Override
            public void onResponse(Call<CategoryResponse> call, Response<CategoryResponse> response) {
                Log.e("success", response.body().getMsg());
                if (response.body().getStatus()) {
                    mData = (ArrayList<Result>) response.body().getResult();
                    mCat_adapter = new Cat_adapter(mData, Home.this);
                    manager = new GridLayoutManager(Home.this, 2);


                    mCat_rv.setAdapter(mCat_adapter);
                    mCat_rv.setLayoutManager(manager);
                    Log.e("array length", mData.size() + "");
                } else {
                    Toast.makeText(Home.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<CategoryResponse> call, Throwable t) {
                Toast.makeText(Home.this, t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    private void initViews() {
        mMenu = findViewById(R.id.menu);
        mMenu.setOnClickListener(this);
        mDrawer = findViewById(R.id.drawer);
        mNavigationView = findViewById(R.id.nav_view_home);
        mNavigationView.setNavigationItemSelectedListener(this);
        mMore_Vert = findViewById(R.id.more_vert) ;
        mMore_Vert.setOnClickListener(this);

        mCat_rv = findViewById(R.id.rv_cat);


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.menu:
                OpenDrawer();
                break;
            case  R.id.more_vert :
                OpenPopUp() ;
                break;
        }
    }

    private void OpenDrawer() {
        mDrawer.openDrawer(Gravity.LEFT);
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.nav_Home:

                break;
            case R.id.nav_profile:

                break;
            case R.id.nav_favorites:
                startActivity(new Intent(Home.this, Favorite_Screen.class));
                break;
            case R.id.setting:
                startActivity(new Intent(Home.this, Setting.class));
                break;
            case R.id.log_out:
                break;
        }
        mDrawer.closeDrawer(Gravity.LEFT);
        return true;
    }

    private PopupWindow OpenPopUp() {
        try {

            mLayoutinfalter = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View layout = mLayoutinfalter.inflate(R.layout.dropdown_menu, null);


            TextView mMyLocation = layout.findViewById(R.id.my_location);
            mMyLocation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                startActivity(new Intent(Home.this , MapsActivity.class));
                }
            });

            layout.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            float scale = getResources().getDisplayMetrics().scaledDensity;
            mDropdown = new PopupWindow(layout, (int) (155 * scale), (int) (50 * scale), true);
            Drawable background = getResources().getDrawable(R.drawable.popup_background);
            mDropdown.setBackgroundDrawable(background);
            mDropdown.showAsDropDown(mMenu, 5, 5);


        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return mDropdown;
    }
}
