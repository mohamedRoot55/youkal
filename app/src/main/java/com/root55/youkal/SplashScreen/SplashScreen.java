package com.root55.youkal.SplashScreen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.root55.youkal.Auth.Login;
import com.root55.youkal.Home.Home;
import com.root55.youkal.R;
import com.root55.youkal.UserSession.User_Session;

public class SplashScreen extends AppCompatActivity {
    private User_Session user_session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);
        user_session = new User_Session(SplashScreen.this);
        MoveToNext();

    }

    private void MoveToNext() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                if(user_session.isLogin()){
                    startActivity(new Intent(SplashScreen.this , Home.class));
                }else {
                    startActivity(new Intent(SplashScreen.this , Login.class));
                }
            }
        }, 4000);
    }
}
