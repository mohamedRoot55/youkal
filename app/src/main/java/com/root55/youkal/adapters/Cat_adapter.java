package com.root55.youkal.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.root55.youkal.Home.Pojos.Result;
import com.root55.youkal.R;

import java.util.ArrayList;

public class Cat_adapter extends RecyclerView.Adapter<Cat_adapter.Cat_Holder> {

    ArrayList<Result> mCategories;
    Context context ;


    public Cat_adapter(ArrayList<Result> mCategories , Context context ) {
        this.mCategories = mCategories;
        this.context = context ;

    }

    @NonNull
    @Override
    public Cat_Holder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cat_item, parent, false);
        return new Cat_Holder(view);

    }

    @Override
    public void onBindViewHolder(@NonNull Cat_Holder holder, int position) {
        holder.mCatName.setText(mCategories.get(position).getName());
       Glide.with(context).load(mCategories.get(position).getImage()).into(holder.mCatImg) ;

    }

    @Override
    public int getItemCount() {
        return mCategories.size();
    }

    public class Cat_Holder extends RecyclerView.ViewHolder {
        ImageView mCatImg;
        TextView mCatName;

        public Cat_Holder(@NonNull View itemView) {
            super(itemView);
            mCatImg = itemView.findViewById(R.id.cat_item_img);
            mCatName = itemView.findViewById(R.id.cat_item_name);

        }
    }
}
