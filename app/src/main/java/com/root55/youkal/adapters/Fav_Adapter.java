package com.root55.youkal.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.root55.youkal.Models.Fav_Model;
import com.root55.youkal.R;

import java.util.ArrayList;

public class Fav_Adapter extends RecyclerView.Adapter<Fav_Adapter.Fav_ViewHolder> {

    ArrayList<Fav_Model> Data ;

    public Fav_Adapter(ArrayList<Fav_Model> data) {
        Data = data;
    }

    @NonNull
    @Override
    public Fav_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
       View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.fav_item , parent , false) ;
       Fav_ViewHolder fav_viewHolder = new Fav_ViewHolder(view) ;
        return fav_viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Fav_ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return Data.size();
    }

    public  class Fav_ViewHolder extends RecyclerView.ViewHolder {

        public Fav_ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
