package com.root55.youkal.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.root55.youkal.Models.Setting_model;
import com.root55.youkal.R;

import java.util.ArrayList;

public class Setting_adapter extends RecyclerView.Adapter<Setting_adapter.Setting_ViewHolder> {
    ArrayList<Setting_model> mSetting_List;
    SelectedItem selectedItem;

    public Setting_adapter(ArrayList<Setting_model> mSetting_List , SelectedItem selectedItem ) {
        this.mSetting_List = mSetting_List;
      this.selectedItem = selectedItem ;
    }

    @NonNull
    @Override
    public Setting_ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.setting_item, parent, false);
        Setting_ViewHolder viewHolder = new Setting_ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull Setting_ViewHolder holder, final int position) {
        holder.title.setText(mSetting_List.get(position).getItem_title());
        holder.setting_icon.setImageResource(mSetting_List.get(position).getDescripe_img());
        holder.go_icon.setImageResource(mSetting_List.get(position).getGo_item());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedItem.SelectedTitle(mSetting_List.get(position).getItem_title());
            }
        });


    }

    @Override
    public int getItemCount() {
        return mSetting_List.size();
    }

    public class Setting_ViewHolder extends RecyclerView.ViewHolder {

        ImageView setting_icon, go_icon;
        TextView title;

        public Setting_ViewHolder(@NonNull View itemView) {
            super(itemView);
            setting_icon = itemView.findViewById(R.id.setting_item_icon);
            go_icon = itemView.findViewById(R.id.setting_item_go);
            title = itemView.findViewById(R.id.setting_item_title);
        }
    }

    public interface SelectedItem {
       public  void SelectedTitle(String title);
    }
}
