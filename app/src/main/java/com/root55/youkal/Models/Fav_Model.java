package com.root55.youkal.Models;

public class Fav_Model {
    final String Title ;
    final int Fav_img ;
    boolean isFav ;

    public Fav_Model(String title, int fav_img, boolean isFav) {
        Title = title;
        Fav_img = fav_img;
        this.isFav = isFav;
    }

    public String getTitle() {
        return Title;
    }

    public int getFav_img() {
        return Fav_img;
    }

    public boolean isFav() {
        return isFav;
    }
}
