package com.root55.youkal.Models;

public class Setting_model {
    private int descripe_img ;
    private String item_title ;
    private int go_item ;

    public Setting_model(int descripe_img, String item_title, int go_item) {
        this.descripe_img = descripe_img;
        this.item_title = item_title;
        this.go_item = go_item;
    }

    public int getDescripe_img() {
        return descripe_img;
    }

    public String getItem_title() {
        return item_title;
    }

    public int getGo_item() {
        return go_item;
    }
}
