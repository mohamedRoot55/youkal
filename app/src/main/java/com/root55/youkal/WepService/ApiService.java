package com.root55.youkal.WepService;

import com.root55.youkal.Auth.Pojos.AuthResponse;
import com.root55.youkal.Home.Pojos.CategoryResponse;
import com.root55.youkal.edit_profile.Pojo.EditProfileResponse;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ApiService {

    @FormUrlEncoded
    @POST("register")
    Call<AuthResponse> Register(@Field("first_name") String first,
                                @Field("last_name") String last,
                                @Field("phone") String phone,
                                @Field("email") String email,
                                @Field("password") String password);

    @FormUrlEncoded
    @POST("login")
    Call<AuthResponse> Login(@Field("email") String email,
                             @Field("password") String password);

    @FormUrlEncoded
    @POST("/user/{user_id}/edit")
    Call<EditProfileResponse> EditProfile(@Path("user_id") int user_id,
                                          @Field("first_name") String first_name,
                                          @Field("last_name") String last_name,
                                          @Field("phone") String phone,
                                          @Field("email") String email);

    @GET("categories?lang=en")
    Call<CategoryResponse> getCategory() ;


}
