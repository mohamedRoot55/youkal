package com.root55.youkal.favorite;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.navigation.NavigationView;
import com.root55.youkal.Home.Home;
import com.root55.youkal.Models.Fav_Model;
import com.root55.youkal.R;
import com.root55.youkal.Setting.Setting;
import com.root55.youkal.adapters.Fav_Adapter;

import java.util.ArrayList;

public class Favorite_Screen extends AppCompatActivity implements View.OnClickListener , NavigationView.OnNavigationItemSelectedListener {

    private DrawerLayout mDrawer ;
    private ImageView mHamburger_icon ;
    private RecyclerView mFav_rv ;
    private ArrayList<Fav_Model> mData ;
    private RecyclerView.LayoutManager manager;
    private Fav_Adapter mAdapter ;
    private NavigationView mNav_fav ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorite__screen);
        initData();
        initViews();
    }

    private void initData() {
        mData = new ArrayList<>() ;
        mData.add(new Fav_Model("pizza" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("shawrma" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("pasta" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("meat" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("cheicken" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("pizza" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("pizza" , R.drawable.arctic_fox ,true)) ;
        mData.add(new Fav_Model("pizza" , R.drawable.arctic_fox ,true)) ;
    }

    private void initViews() {
        mDrawer = findViewById(R.id.fav_drawer) ;
        mHamburger_icon = findViewById(R.id.menu) ;
        mFav_rv = findViewById(R.id.rv_fav) ;
        mNav_fav = findViewById(R.id.nav_view_fav) ;
        manager = new LinearLayoutManager(Favorite_Screen.this , RecyclerView.VERTICAL , false );
        mAdapter = new Fav_Adapter(mData) ;
        mFav_rv.setLayoutManager(manager);
        mFav_rv.setAdapter(mAdapter);

        mNav_fav.setNavigationItemSelectedListener(this);
        mHamburger_icon.setOnClickListener(this);

    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(Gravity.LEFT)) {
            mDrawer.closeDrawer(Gravity.LEFT);
        } else {
            super.onBackPressed();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.menu :
                mDrawer.openDrawer(Gravity.LEFT);
                break;
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {
        switch (menuItem.getItemId()){
            case  R.id.nav_Home :
                startActivity(new Intent(Favorite_Screen.this , Home.class));
                break;
            case R.id.nav_profile :

                break;
            case  R.id.nav_favorites :


                break;
            case  R.id.setting :
                startActivity(new Intent(Favorite_Screen.this , Setting.class));
                break;
            case R.id.log_out :
                break;
        }
        mDrawer.closeDrawer(Gravity.LEFT);
        return true;
    }
}
