package com.root55.youkal.edit_profile;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.root55.youkal.Auth.Login;
import com.root55.youkal.R;
import com.root55.youkal.Setting.Setting;
import com.root55.youkal.UserSession.User_Session;
import com.root55.youkal.WepService.ApiService;
import com.root55.youkal.WepService.WepServiceClient;
import com.root55.youkal.edit_profile.Pojo.EditProfileResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Edit_Profile extends AppCompatActivity implements View.OnClickListener {
    User_Session mUser_session;
    String mFirstName, mLastName, mEmail, mPhone;
    String mUserId ;

    ImageView mSelectImg;
    EditText mFirstName_et, mLastName_et, mEmail_et, mPhone_et;
    Button mSave;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit__profile);
        getSavedData();
        initViews();
    }

    private void initViews() {
        mSelectImg = findViewById(R.id.select_img);
        mFirstName_et = findViewById(R.id.first_name);
        mLastName_et = findViewById(R.id.last_name);
        mEmail_et = findViewById(R.id.email);
        mPhone_et = findViewById(R.id.phone);
        mSave = findViewById(R.id.save_btn);

       mUserId =  mUser_session.getUserid();

        mFirstName_et.setText(mFirstName);
        mLastName_et.setText(mLastName);
        mEmail_et.setText(mEmail);
        mPhone_et.setText(mPhone);

        mSelectImg.setOnClickListener(this);

        mSave.setOnClickListener(this);

    }

    private void getSavedData() {
        mUser_session = new User_Session(Edit_Profile.this);
        // HashMap<String, String> UserData = mUser_session.getUserData();
        mFirstName = mUser_session.getUserData().get(User_Session.USER_FIRST_NAME_KEY);
        mLastName = mUser_session.getUserData().get(User_Session.USER_LAST_NAME_KEY);
        mEmail = mUser_session.getUserData().get(User_Session.USER_EMAIL_KEY);
        mPhone = mUser_session.getUserData().get(User_Session.USER_PHONE_KEY);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.select_img:
                break;

            case R.id.save_btn:
                if (CheckValidation()) {
                   //  CallEditProfileApi();
                }
                break;
        }

    }

    private void CallEditProfileApi() {



        ApiService apiService = WepServiceClient.getRetrofit().create(ApiService.class);
        Call<EditProfileResponse> call = apiService.EditProfile(
                Integer.parseInt(mUserId),
                mFirstName,
                mLastName,
                mPhone,
                mEmail
        );
        call.enqueue(new Callback<EditProfileResponse>() {
            @Override
            public void onResponse(Call<EditProfileResponse> call, Response<EditProfileResponse> response) {

                Log.e("success" , response.body().getStatus().toString()) ;
                Log.e("success" , response.body().getMsg()) ;
//                assert response.body() != null;
//                if (response.body().getStatus()) {
//
//                    mUser_session.EditProfileData(mFirstName_et.getText().toString()
//                            , mLastName_et.getText().toString(), mEmail_et.getText().toString()
//                            , mPhone_et.getText().toString());
//                    Snackbar.make(findViewById(android.R.id.content), "data is saved", Snackbar.LENGTH_SHORT).show();
//                    new Handler().postDelayed(new Runnable() {
//                        @Override
//                        public void run() {
//                            startActivity(new Intent(Edit_Profile.this, Setting.class));
//                        }
//                    }, 3000);
//
//
//                } else {
//                    Toast.makeText(Edit_Profile.this, response.body().getMsg(), Toast.LENGTH_LONG).show();
//
//                }
            }

            @Override
            public void onFailure(Call<EditProfileResponse> call, Throwable t) {
                Log.e("error" , t.getMessage()) ;
                Toast.makeText(Edit_Profile.this, t.getMessage(), Toast.LENGTH_LONG).show();

            }
        });
    }

    boolean CheckValidation() {
        if (mFirstName_et.getText().toString().isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content), "first name is requierd", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (mLastName_et.getText().toString().isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content), "last name is requierd", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (mEmail_et.getText().toString().isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content), "email is requierd", Snackbar.LENGTH_SHORT).show();
            return false;
        } else if (mPhone_et.getText().toString().isEmpty()) {
            Snackbar.make(findViewById(android.R.id.content), "phone is requierd", Snackbar.LENGTH_SHORT).show();
            return false;
        }

        return true;
    }
}


