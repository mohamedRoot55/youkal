package com.root55.youkal.UserSession;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;

import java.util.HashMap;

public class User_Session {
    SharedPreferences mSharedPreferences;
    SharedPreferences.Editor mEditor;
    Context mContext;


    private static final String FILE_NAME = "Youkal";

    public static final String USER_EMAIL_KEY = "EMAIL";
    public static final String USER_PASSWORD_KEY = "PASSWORD";
    public static final String USER_FIRST_NAME_KEY = "FIRST NAME";
    public static final String USER_LAST_NAME_KEY = "LAST NAME";
    public static final String USER_PHONE_KEY = "PHONE";
    public static final String LOGIN_STATUS = "LOGIN_STATUS";
    public static final  String USERID = "USER_ID" ;

    @SuppressLint("CommitPrefEdits")
    public User_Session(Context mContext) {
        this.mContext = mContext;
        mSharedPreferences = mContext.getSharedPreferences(FILE_NAME, Context.MODE_PRIVATE);
        mEditor = mSharedPreferences.edit();
    }

    public void Login(String Email, String password, boolean login_status) {
        mEditor.putString(USER_EMAIL_KEY, Email);
        mEditor.putString(USER_PASSWORD_KEY, password);
        mEditor.putBoolean(LOGIN_STATUS, login_status);
        mEditor.apply();
    }

    public void SignIn(String Email, String password, String phone, String firstName, String lastName, boolean login_status) {

        mEditor.putString(USER_EMAIL_KEY, Email);
        mEditor.putString(USER_PASSWORD_KEY, password);
        mEditor.putString(USER_LAST_NAME_KEY, lastName);
        mEditor.putString(USER_FIRST_NAME_KEY, firstName);
        mEditor.putString(USER_PHONE_KEY, phone);
        mEditor.putBoolean(LOGIN_STATUS, login_status);

        mEditor.apply();
    }

    public void EditProfileData(String firstName, String lastName, String email, String phone) {
        mEditor.putString(USER_FIRST_NAME_KEY, firstName);
        mEditor.putString(USER_LAST_NAME_KEY, lastName);
        mEditor.putString(USER_EMAIL_KEY, email);
        mEditor.putString(USER_PHONE_KEY, phone);
        mEditor.apply();
    }

    public HashMap<String, String> getUserData() {

        HashMap<String, String> Data = new HashMap<>();
        Data.put(USER_EMAIL_KEY, mSharedPreferences.getString(USER_EMAIL_KEY, ""));
        Data.put(USER_PASSWORD_KEY, mSharedPreferences.getString(USER_PASSWORD_KEY, ""));
        Data.put(USER_FIRST_NAME_KEY, mSharedPreferences.getString(USER_FIRST_NAME_KEY, ""));
        Data.put(USER_LAST_NAME_KEY, mSharedPreferences.getString(USER_LAST_NAME_KEY, ""));
        Data.put(USER_PHONE_KEY, mSharedPreferences.getString(USER_PHONE_KEY, ""));
        return Data;
    }
    public void addUserId(String userId){
        mEditor.putString(USERID , userId) ;
        mEditor.apply();

    }
    public String getUserid(){
        String userId = mSharedPreferences.getString(USERID , "0" );
        return userId ;
    }


    public boolean isLogin() {
        return mSharedPreferences.getBoolean(LOGIN_STATUS, false);
    }
}
